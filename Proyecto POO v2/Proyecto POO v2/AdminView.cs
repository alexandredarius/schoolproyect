﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using Proyecto_POO_v2.Clases;

namespace Proyecto_POO_v2
{
    public partial class AdminView : Form
    {
        public AdminView()
        {
            InitializeComponent();
        }
        private void SubmitEmployee_Click(object sender, EventArgs e)
        {
            RegistrarEmpleados();
        }

        private void SubmitProduct_Click(object sender, EventArgs e)
        {
            RegistrarProductos();
        }
        public void RegistrarProductos()
        {
            Producto producto = new Producto();
            producto.NombreProducto = ProductNameField.Text;
            producto.PrecioProducto = ProductPriceField.Text;
            producto.DetallesProducto = ProductDetailsField.Text;

            string productoaguardar = producto.NombreProducto + "," + producto.PrecioProducto + "," + producto.DetallesProducto + "\n";

            File.AppendAllText("DBproductos.txt", productoaguardar);
        }
        public void RegistrarEmpleados()
        {
            Empleado empleado = new Empleado();
            empleado.Nombre = EmployeeNameField.Text;
            empleado.Telefono = EmployeePhoneField.Text;
            empleado.Puesto = EmployeeChargeField.Text;

            string empleadoaguardar = empleado.Nombre + "," + empleado.Telefono + "," + empleado.Puesto + "\n";

            File.AppendAllText("DBEmpleados.txt", empleadoaguardar);
        }
        public List<Empleado> LeerEmpleados()
        {
            List<Empleado> empleados = new List<Empleado>();
            foreach (string line in File.ReadAllLines("DBEmpleados.txt"))
            {
                string[] cadena = line.Split(',');
                Empleado empleado = new Empleado();
                empleado.Nombre = cadena[0];
                empleado.Telefono = cadena[1];
                empleado.Puesto = cadena[2];
                empleados.Add(empleado);
            }
            return empleados;
        }

        private void UpdateEmployeeList_Click(object sender, EventArgs e)
        {
            foreach (Empleado item in LeerEmpleados())
            {
                EmployeeList.Items.Add(item.Nombre + " " + item.Puesto + " " + item.Telefono);
            }
            EmployeeList.Update();
        }

        //la lista de pedidos aun no es actualizable
        private void UpdateQueryList_Click(object sender, EventArgs e)
        {
            //foreach (Pedido item in LeerPedidos())
            //{
            //    EmployeeList.Items.Add(item.Nombre + " " + item.Puesto + " " + item.Telefono);
            //}
            //QueryList.Update();
        }
    }
}
