﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Proyecto_POO_v2.Clases
{
    public class Producto
    {
        public string NombreProducto { get; set; }
        public string PrecioProducto { get; set; }
        public string DetallesProducto { get; set; }
        public int IdProducto { get; set; }
    }
}
