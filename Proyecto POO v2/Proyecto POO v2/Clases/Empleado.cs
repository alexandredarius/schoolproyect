﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proyecto_POO_v2.Clases
{
    public class Empleado : Persona
    {
        public override string Nombre { get; set; }
        public override string Telefono { get; set; }
        public string Puesto { get; set; }
    }
}
