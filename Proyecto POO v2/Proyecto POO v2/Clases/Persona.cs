﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Proyecto_POO_v2.Clases
{
    public abstract class Persona
    {
        public abstract String Nombre { get; set; }
        public abstract String Telefono { get; set; }

    }
}
