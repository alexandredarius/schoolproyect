﻿namespace Proyecto_POO_v2
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.panel1 = new System.Windows.Forms.Panel();
            this.TitleLabel = new System.Windows.Forms.Label();
            this.PedidoGroup = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.AdminLauncher = new System.Windows.Forms.Button();
            this.SubmitOrder = new System.Windows.Forms.Button();
            this.UserPhone = new System.Windows.Forms.TextBox();
            this.UserName = new System.Windows.Forms.TextBox();
            this.PhoneLabel = new System.Windows.Forms.Label();
            this.NameLabel = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.checkedListBox2 = new System.Windows.Forms.CheckedListBox();
            this.ComidasLabel = new System.Windows.Forms.Label();
            this.panel5 = new System.Windows.Forms.Panel();
            this.CostListLabel = new System.Windows.Forms.Label();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.productoBindingSource2 = new System.Windows.Forms.BindingSource(this.components);
            this.productoBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.productoBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.button1 = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.PedidoGroup.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.productoBindingSource2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.productoBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.productoBindingSource1)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.button1);
            this.panel1.Controls.Add(this.TitleLabel);
            this.panel1.Controls.Add(this.PedidoGroup);
            this.panel1.Controls.Add(this.panel4);
            this.panel1.Controls.Add(this.panel5);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(801, 449);
            this.panel1.TabIndex = 0;
            // 
            // TitleLabel
            // 
            this.TitleLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TitleLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TitleLabel.Location = new System.Drawing.Point(3, 0);
            this.TitleLabel.Name = "TitleLabel";
            this.TitleLabel.Size = new System.Drawing.Size(795, 27);
            this.TitleLabel.TabIndex = 7;
            this.TitleLabel.Text = "Seleccione su pedido";
            this.TitleLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // PedidoGroup
            // 
            this.PedidoGroup.Controls.Add(this.label1);
            this.PedidoGroup.Controls.Add(this.AdminLauncher);
            this.PedidoGroup.Controls.Add(this.SubmitOrder);
            this.PedidoGroup.Controls.Add(this.UserPhone);
            this.PedidoGroup.Controls.Add(this.UserName);
            this.PedidoGroup.Controls.Add(this.PhoneLabel);
            this.PedidoGroup.Controls.Add(this.NameLabel);
            this.PedidoGroup.Location = new System.Drawing.Point(3, 371);
            this.PedidoGroup.Name = "PedidoGroup";
            this.PedidoGroup.Size = new System.Drawing.Size(794, 76);
            this.PedidoGroup.TabIndex = 6;
            this.PedidoGroup.TabStop = false;
            this.PedidoGroup.Text = "Realizar Pedido";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(583, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(125, 13);
            this.label1.TabIndex = 9;
            this.label1.Text = "Boton puesto a proposito";
            // 
            // AdminLauncher
            // 
            this.AdminLauncher.Location = new System.Drawing.Point(573, 42);
            this.AdminLauncher.Name = "AdminLauncher";
            this.AdminLauncher.Size = new System.Drawing.Size(145, 23);
            this.AdminLauncher.TabIndex = 8;
            this.AdminLauncher.Text = "Ventana de Administrador";
            this.AdminLauncher.UseVisualStyleBackColor = true;
            this.AdminLauncher.Click += new System.EventHandler(this.AdminLauncher_Click);
            // 
            // SubmitOrder
            // 
            this.SubmitOrder.Location = new System.Drawing.Point(288, 19);
            this.SubmitOrder.Name = "SubmitOrder";
            this.SubmitOrder.Size = new System.Drawing.Size(85, 46);
            this.SubmitOrder.TabIndex = 7;
            this.SubmitOrder.Text = "Generar Orden";
            this.SubmitOrder.UseVisualStyleBackColor = true;
            this.SubmitOrder.Click += new System.EventHandler(this.SubmitOrder_Click);
            // 
            // UserPhone
            // 
            this.UserPhone.Location = new System.Drawing.Point(123, 45);
            this.UserPhone.Name = "UserPhone";
            this.UserPhone.Size = new System.Drawing.Size(146, 20);
            this.UserPhone.TabIndex = 6;
            // 
            // UserName
            // 
            this.UserName.Location = new System.Drawing.Point(123, 19);
            this.UserName.Name = "UserName";
            this.UserName.Size = new System.Drawing.Size(146, 20);
            this.UserName.TabIndex = 3;
            // 
            // PhoneLabel
            // 
            this.PhoneLabel.AutoSize = true;
            this.PhoneLabel.Location = new System.Drawing.Point(9, 48);
            this.PhoneLabel.Name = "PhoneLabel";
            this.PhoneLabel.Size = new System.Drawing.Size(101, 13);
            this.PhoneLabel.TabIndex = 5;
            this.PhoneLabel.Text = "Ingrese su Telefono";
            // 
            // NameLabel
            // 
            this.NameLabel.AutoSize = true;
            this.NameLabel.Location = new System.Drawing.Point(9, 22);
            this.NameLabel.Name = "NameLabel";
            this.NameLabel.Size = new System.Drawing.Size(96, 13);
            this.NameLabel.TabIndex = 4;
            this.NameLabel.Text = "Indique su Nombre";
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.panel3);
            this.panel4.Controls.Add(this.ComidasLabel);
            this.panel4.Location = new System.Drawing.Point(275, 31);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(522, 337);
            this.panel4.TabIndex = 1;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.checkedListBox2);
            this.panel3.Location = new System.Drawing.Point(0, 33);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(522, 304);
            this.panel3.TabIndex = 1;
            // 
            // checkedListBox2
            // 
            this.checkedListBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.checkedListBox2.FormattingEnabled = true;
            this.checkedListBox2.Location = new System.Drawing.Point(0, 0);
            this.checkedListBox2.Name = "checkedListBox2";
            this.checkedListBox2.Size = new System.Drawing.Size(522, 304);
            this.checkedListBox2.TabIndex = 0;
            // 
            // ComidasLabel
            // 
            this.ComidasLabel.AutoSize = true;
            this.ComidasLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            this.ComidasLabel.Location = new System.Drawing.Point(13, 9);
            this.ComidasLabel.Name = "ComidasLabel";
            this.ComidasLabel.Size = new System.Drawing.Size(121, 16);
            this.ComidasLabel.TabIndex = 8;
            this.ComidasLabel.Text = "Comida disponible";
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.CostListLabel);
            this.panel5.Controls.Add(this.listBox1);
            this.panel5.Location = new System.Drawing.Point(3, 31);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(269, 337);
            this.panel5.TabIndex = 2;
            // 
            // CostListLabel
            // 
            this.CostListLabel.AutoSize = true;
            this.CostListLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            this.CostListLabel.Location = new System.Drawing.Point(9, 9);
            this.CostListLabel.Name = "CostListLabel";
            this.CostListLabel.Size = new System.Drawing.Size(103, 16);
            this.CostListLabel.TabIndex = 1;
            this.CostListLabel.Text = "Lista de precios";
            // 
            // listBox1
            // 
            this.listBox1.FormattingEnabled = true;
            this.listBox1.Location = new System.Drawing.Point(4, 33);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(262, 303);
            this.listBox1.TabIndex = 0;
            // 
            // productoBindingSource2
            // 
            this.productoBindingSource2.DataSource = typeof(Proyecto_POO_v2.Clases.Producto);
            // 
            // productoBindingSource
            // 
            this.productoBindingSource.DataSource = typeof(Proyecto_POO_v2.Clases.Producto);
            // 
            // productoBindingSource1
            // 
            this.productoBindingSource1.DataSource = typeof(Proyecto_POO_v2.Clases.Producto);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(73, 4);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(159, 23);
            this.button1.TabIndex = 2;
            this.button1.Text = "Actualizar Listas";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.Button1_Click);
            // 
            // Form1
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(801, 449);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "Form1";
            this.Text = "Mostrador";
            this.panel1.ResumeLayout(false);
            this.PedidoGroup.ResumeLayout(false);
            this.PedidoGroup.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.productoBindingSource2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.productoBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.productoBindingSource1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Label PhoneLabel;
        private System.Windows.Forms.Label NameLabel;
        private System.Windows.Forms.TextBox UserName;
        private System.Windows.Forms.GroupBox PedidoGroup;
        private System.Windows.Forms.Label TitleLabel;
        private System.Windows.Forms.TextBox UserPhone;
        private System.Windows.Forms.Button SubmitOrder;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label ComidasLabel;
        private System.Windows.Forms.CheckedListBox checkedListBox2;
        private System.Windows.Forms.Label CostListLabel;
        private System.Windows.Forms.BindingSource productoBindingSource;
        private System.Windows.Forms.BindingSource productoBindingSource1;
        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.BindingSource productoBindingSource2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button AdminLauncher;
        private System.Windows.Forms.Button button1;
    }
}

