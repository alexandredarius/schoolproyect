﻿namespace Proyecto_POO_v2
{
    partial class AdminView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.UpdateEmployeeList = new System.Windows.Forms.Button();
            this.UpdateQueryList = new System.Windows.Forms.Button();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.EmployeeList = new System.Windows.Forms.ListBox();
            this.QueryList = new System.Windows.Forms.ListBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.ProductDetailsField = new System.Windows.Forms.TextBox();
            this.ProductPriceField = new System.Windows.Forms.TextBox();
            this.ProductNameField = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.SubmitProduct = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.EmployeeChargeField = new System.Windows.Forms.TextBox();
            this.EmployeePhoneField = new System.Windows.Forms.TextBox();
            this.EmployeeNameField = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.SubmitEmployee = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel2.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.panel3);
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(800, 450);
            this.panel1.TabIndex = 0;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.UpdateEmployeeList);
            this.panel3.Controls.Add(this.UpdateQueryList);
            this.panel3.Controls.Add(this.label9);
            this.panel3.Controls.Add(this.label8);
            this.panel3.Controls.Add(this.EmployeeList);
            this.panel3.Controls.Add(this.QueryList);
            this.panel3.Location = new System.Drawing.Point(289, 4);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(508, 443);
            this.panel3.TabIndex = 1;
            // 
            // UpdateEmployeeList
            // 
            this.UpdateEmployeeList.Location = new System.Drawing.Point(369, 3);
            this.UpdateEmployeeList.Name = "UpdateEmployeeList";
            this.UpdateEmployeeList.Size = new System.Drawing.Size(117, 23);
            this.UpdateEmployeeList.TabIndex = 5;
            this.UpdateEmployeeList.Text = "Actualizar Empleados";
            this.UpdateEmployeeList.UseVisualStyleBackColor = true;
            this.UpdateEmployeeList.Click += new System.EventHandler(this.UpdateEmployeeList_Click);
            // 
            // UpdateQueryList
            // 
            this.UpdateQueryList.Location = new System.Drawing.Point(123, 3);
            this.UpdateQueryList.Name = "UpdateQueryList";
            this.UpdateQueryList.Size = new System.Drawing.Size(107, 23);
            this.UpdateQueryList.TabIndex = 4;
            this.UpdateQueryList.Text = "Actualizar Pedidos";
            this.UpdateQueryList.UseVisualStyleBackColor = true;
            this.UpdateQueryList.Click += new System.EventHandler(this.UpdateQueryList_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(264, 8);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(99, 13);
            this.label9.TabIndex = 3;
            this.label9.Text = "Lista de Empleados";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(3, 8);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(85, 13);
            this.label8.TabIndex = 2;
            this.label8.Text = "Lista de Pedidos";
            // 
            // EmployeeList
            // 
            this.EmployeeList.FormattingEnabled = true;
            this.EmployeeList.Location = new System.Drawing.Point(267, 28);
            this.EmployeeList.Name = "EmployeeList";
            this.EmployeeList.Size = new System.Drawing.Size(238, 407);
            this.EmployeeList.TabIndex = 1;
            // 
            // QueryList
            // 
            this.QueryList.FormattingEnabled = true;
            this.QueryList.Location = new System.Drawing.Point(3, 28);
            this.QueryList.Name = "QueryList";
            this.QueryList.Size = new System.Drawing.Size(258, 407);
            this.QueryList.TabIndex = 0;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.groupBox2);
            this.panel2.Controls.Add(this.groupBox1);
            this.panel2.Location = new System.Drawing.Point(3, 3);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(282, 444);
            this.panel2.TabIndex = 0;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.ProductDetailsField);
            this.groupBox2.Controls.Add(this.ProductPriceField);
            this.groupBox2.Controls.Add(this.ProductNameField);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.SubmitProduct);
            this.groupBox2.Location = new System.Drawing.Point(3, 206);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(276, 235);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Registrar Productos";
            // 
            // ProductDetailsField
            // 
            this.ProductDetailsField.Location = new System.Drawing.Point(95, 135);
            this.ProductDetailsField.Name = "ProductDetailsField";
            this.ProductDetailsField.Size = new System.Drawing.Size(100, 20);
            this.ProductDetailsField.TabIndex = 7;
            // 
            // ProductPriceField
            // 
            this.ProductPriceField.Location = new System.Drawing.Point(95, 94);
            this.ProductPriceField.Name = "ProductPriceField";
            this.ProductPriceField.Size = new System.Drawing.Size(100, 20);
            this.ProductPriceField.TabIndex = 6;
            // 
            // ProductNameField
            // 
            this.ProductNameField.Location = new System.Drawing.Point(95, 52);
            this.ProductNameField.Name = "ProductNameField";
            this.ProductNameField.Size = new System.Drawing.Size(100, 20);
            this.ProductNameField.TabIndex = 5;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(12, 135);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(45, 13);
            this.label7.TabIndex = 4;
            this.label7.Text = "Detalles";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(12, 94);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(37, 13);
            this.label6.TabIndex = 3;
            this.label6.Text = "Precio";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(12, 52);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(44, 13);
            this.label5.TabIndex = 2;
            this.label5.Text = "Nombre";
            // 
            // SubmitProduct
            // 
            this.SubmitProduct.Location = new System.Drawing.Point(85, 193);
            this.SubmitProduct.Name = "SubmitProduct";
            this.SubmitProduct.Size = new System.Drawing.Size(110, 23);
            this.SubmitProduct.TabIndex = 0;
            this.SubmitProduct.Text = "Registrar Producto";
            this.SubmitProduct.UseVisualStyleBackColor = true;
            this.SubmitProduct.Click += new System.EventHandler(this.SubmitProduct_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.EmployeeChargeField);
            this.groupBox1.Controls.Add(this.EmployeePhoneField);
            this.groupBox1.Controls.Add(this.EmployeeNameField);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.SubmitEmployee);
            this.groupBox1.Location = new System.Drawing.Point(3, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(276, 197);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Registrar Empleados";
            // 
            // EmployeeChargeField
            // 
            this.EmployeeChargeField.Location = new System.Drawing.Point(95, 109);
            this.EmployeeChargeField.Name = "EmployeeChargeField";
            this.EmployeeChargeField.Size = new System.Drawing.Size(100, 20);
            this.EmployeeChargeField.TabIndex = 6;
            // 
            // EmployeePhoneField
            // 
            this.EmployeePhoneField.Location = new System.Drawing.Point(95, 70);
            this.EmployeePhoneField.Name = "EmployeePhoneField";
            this.EmployeePhoneField.Size = new System.Drawing.Size(100, 20);
            this.EmployeePhoneField.TabIndex = 5;
            // 
            // EmployeeNameField
            // 
            this.EmployeeNameField.Location = new System.Drawing.Point(95, 33);
            this.EmployeeNameField.Name = "EmployeeNameField";
            this.EmployeeNameField.Size = new System.Drawing.Size(100, 20);
            this.EmployeeNameField.TabIndex = 4;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(7, 109);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(40, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "Puesto";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(7, 70);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(49, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Telefono";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(7, 33);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(44, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Nombre";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(7, 128);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(0, 13);
            this.label4.TabIndex = 1;
            // 
            // SubmitEmployee
            // 
            this.SubmitEmployee.Location = new System.Drawing.Point(85, 144);
            this.SubmitEmployee.Name = "SubmitEmployee";
            this.SubmitEmployee.Size = new System.Drawing.Size(110, 23);
            this.SubmitEmployee.TabIndex = 0;
            this.SubmitEmployee.Text = "Registrar Empleado";
            this.SubmitEmployee.UseVisualStyleBackColor = true;
            this.SubmitEmployee.Click += new System.EventHandler(this.SubmitEmployee_Click);
            // 
            // AdminView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.panel1);
            this.Name = "AdminView";
            this.Text = "AdminView";
            this.panel1.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.ListBox EmployeeList;
        private System.Windows.Forms.ListBox QueryList;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button SubmitProduct;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button SubmitEmployee;
        private System.Windows.Forms.Button UpdateEmployeeList;
        private System.Windows.Forms.Button UpdateQueryList;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox ProductDetailsField;
        private System.Windows.Forms.TextBox ProductPriceField;
        private System.Windows.Forms.TextBox ProductNameField;
        private System.Windows.Forms.TextBox EmployeeChargeField;
        private System.Windows.Forms.TextBox EmployeePhoneField;
        private System.Windows.Forms.TextBox EmployeeNameField;
    }
}