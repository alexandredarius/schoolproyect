﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Proyecto_POO_v2.Clases;
using System.IO;

namespace Proyecto_POO_v2
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            AddItems();
        }
        public void AddItems()
        {
            checkedListBox2.CheckOnClick = true;
            Producto producto = new Producto();
            foreach (Producto item in LeerProductos())
            {
                listBox1.Items.Add(item.NombreProducto + " $" + item.PrecioProducto);

                checkedListBox2.Items.Add(item.NombreProducto);
            }
        }
        private void SubmitOrder_Click(object sender, EventArgs e)
        {
            GenerarOrden();
            RegistrarPedido();
            MessageBox.Show("Se ha generado su pedido\nEspere el despacho de su orden");
        }
        public Pedido GenerarOrden()
        {
            Pedido pedido = new Pedido();
            Cliente nuevoCliente = new Cliente();
            nuevoCliente.Nombre = UserName.ToString();
            nuevoCliente.Telefono = UserPhone.ToString();
            pedido.Cliente = nuevoCliente;
            foreach (int item in checkedListBox2.CheckedIndices)
            {
                //pedido.Lista.Add();
            }
            return pedido;
        }
        public void RegistrarPedido()
        {
            var registro = GenerarOrden();

            Cliente cliente = registro.Cliente;
            var lista = registro.Lista;

            var listaProductos = LeerProductos();
            List<Producto> nLProductos = new List<Producto>();
            foreach (int item in checkedListBox2.SelectedIndices)
            {
                nLProductos.Add(listaProductos[item]);
            }
            Producto uno = nLProductos[0];
            Producto dos = nLProductos[1];
            Producto tres = nLProductos[2];
            
            string pedidoaguardar = "Cliente: " + cliente.Nombre + "," + cliente.Telefono + ","+uno.NombreProducto+ ","+dos.NombreProducto + ","+tres.NombreProducto+ "\n";
            File.AppendAllText("DBPedidos.txt", pedidoaguardar);

        }
        public List<Producto> LeerProductos()
        {
            List<Producto> productos = new List<Producto>();

            foreach (string line in File.ReadAllLines("DBproductos.txt"))
            {
                string[] cadena = line.Split(',');
                Producto producto = new Producto();
                producto.NombreProducto = cadena[0];
                producto.PrecioProducto = cadena[1];
                producto.DetallesProducto = cadena[2];
                productos.Add(producto);
            }
            return productos;
        }

        private void AdminLauncher_Click(object sender, EventArgs e)
        {
            AdminView admin = new AdminView();
            admin.Show();
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            foreach (Producto item in LeerProductos())
            {
                listBox1.Items.Add(item.NombreProducto + " $" + item.PrecioProducto);

                checkedListBox2.Items.Add(item.NombreProducto);
            }
            checkedListBox2.Update();
            listBox1.Update();
        }
    }
}
